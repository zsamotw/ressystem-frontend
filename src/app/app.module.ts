import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FullCalendarModule } from 'ng-fullcalendar';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { NewTeacherComponent } from './components/new-teacher/new-teacher.component';
import { TeachersComponent } from './components/teachers/teachers.component';
import { TeacherDetailsComponent } from './components/teacher-details/teacher-details.component';
import { WorkshopsComponent } from './components/workshops/workshops.component';
import { WorkshopDetailsComponent } from './components/workshop-details/workshop-details.component';
import { NewWorkshopComponent } from './components/new-workshop/new-workshop.component';
import { WorkshopSearchComponent } from './components/workshop-search/workshop-search.component';
import { WorkshopAddToCalendarComponent } from './components/workshop-add-to-calendar/workshop-add-to-calendar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-modal';
import { WorkshopComponent } from './components/workshop/workshop.component';
import { ItemsComponent } from './components/items/items.component';
import { ItemComponent } from './components/item/item.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { ItemDetailsComponent } from './components/item-details/item-details.component';
import { InputTextModule } from 'primeng/inputtext';
import { TabViewModule } from 'primeng/tabview';
import { NavbarComponent } from './components/navbar/navbar.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        AdminMenuComponent,
        NewTeacherComponent,
        NewWorkshopComponent,
        TeachersComponent,
        TeacherDetailsComponent,
        WorkshopsComponent,
        WorkshopDetailsComponent,
        WorkshopSearchComponent,
        WorkshopAddToCalendarComponent,
        WorkshopComponent,
        ItemsComponent,
        ItemComponent,
        TeacherComponent,
        ItemDetailsComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        ModalModule,
        FullCalendarModule,
        InputTextModule,
        TabViewModule,
        ConfirmDialogModule,
        BrowserAnimationsModule,
        HttpClientInMemoryWebApiModule.forRoot(
            InMemoryDataService, { dataEncapsulation: false }
        ),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
