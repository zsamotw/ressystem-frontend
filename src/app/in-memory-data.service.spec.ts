import { TestBed } from '@angular/core/testing';

import { InMemoryDataTeacherService } from './in-memory-data-teacher.service';

describe('InMemoryDataTeacherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InMemoryDataTeacherService = TestBed.get(InMemoryDataTeacherService);
    expect(service).toBeTruthy();
  });
});
