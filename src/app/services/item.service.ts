import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Item } from '../models/item';

@Injectable({
    providedIn: 'root'
})
export class ItemService {
    private itemsUrl = 'api/items';
    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getItems(): Observable<Item[]> {
        return this.http.get<Item[]>(this.itemsUrl)
        .pipe(
            catchError(this.handleError('get workshop', []))
        );
    }

    getItem(id: number): Observable<Item> {
        const url = '${this.itemsUrl}/${id}';
        return this.http.get<Item>(url)
        .pipe(
            catchError(this.handleError<Item>('get workshop'))
        );
    }

	addItem(item: Item): Observable<Item> {
		return this.http.post<Item>(this.itemsUrl, item, this.httpOptions)
        .pipe(
            catchError(this.handleError<Item>('add workshop'))
        )
	}

    // updateWorkshop(workshop: Workshop): Observable<any> {
    //     return this.http.post<Workshop>(this.workshopsUrl, workshop, this.httpOptions)
    //     .pipe(
    //         catchError(this.handleError<any>('update workshop'))
    //     )
    // }

	// deleteWorkshop(workshop: Workshop | number): Observable<Workshop> {
		// const id = typeof workshop === 'number' ? workshop : workshop.id;
		// const url = `${this.workshopsUrl}/${id}`;
		// return this.http.delete<Workshop>(url, this.httpOptions)
    //     .pipe(
    //         catchError(this.handleError<Workshop>('delete workshop'))
    //     )
	// }

    // searchWorkshop(query: string): Observable<Workshop[]> {
    //     if(!query) {
    //         return this.getWorkshops();
    //     }
    //     const url = `${this.workshopsUrl}/?description=${query}`;
    //     console.log(url)
    //     return this.http.get<Workshop[]>(url).pipe(
    //         catchError(this.handleError<Workshop[]>('search workshop'))
    // //     )
    // }

    constructor(private http: HttpClient) { }

}
