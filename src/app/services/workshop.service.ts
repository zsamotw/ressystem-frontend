import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Workshop } from '../models/workshop';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class WorkshopService {
    private workshopsUrl = 'api/workshops';
    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getWorkshops(): Observable<Workshop[]> {
        return this.http.get<Workshop[]>(this.workshopsUrl)
            .pipe(
                catchError(this.handleError('get workshop', []))
            );
    }

    getWorkshop(id: number): Observable<Workshop> {
        const url = '${this.workshopsUrl}/${id}';
        return this.http.get<Workshop>(url)
            .pipe(
                catchError(this.handleError<Workshop>('get workshop'))
            );
    }

    addWorkshop(workshop: Workshop): Observable<Workshop> {
        return this.http.post<Workshop>(this.workshopsUrl, workshop, this.httpOptions)
            .pipe(
                catchError(this.handleError<Workshop>('add workshop'))
            );
    }

    updateWorkshop(workshop: Workshop): Observable<any> {
        return this.http.post<Workshop>(this.workshopsUrl, workshop, this.httpOptions)
            .pipe(
                catchError(this.handleError<any>('update workshop'))
            );
    }

    deleteWorkshop(workshop: Workshop | number): Observable<Workshop> {
        const id = typeof workshop === 'number' ? workshop : workshop.id;
        const url = `${this.workshopsUrl}/${id}`;
        return this.http.delete<Workshop>(url, this.httpOptions)
            .pipe(
                catchError(this.handleError<Workshop>('delete workshop'))
            );
    }

    searchWorkshop(query: string): Observable<Workshop[]> {
        if (!query) {
            return this.getWorkshops();
        }
        const url = `${this.workshopsUrl}/?description=${query}`;
        console.log(url)
        return this.http.get<Workshop[]>(url).pipe(
            catchError(this.handleError<Workshop[]>('search workshop'))
        );
    }

    constructor(private http: HttpClient) { }

}
