import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Teacher } from '../models/teacher';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class TeacherService {
    private teachersUrl = 'https://localhost:5001/api/teachers';
    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getTeachers(): Observable<Teacher[]> {
        return this.http.get<Teacher[]>(this.teachersUrl)
        .pipe(
            catchError(this.handleError('getTeacher', []))
        );
    }

    getTeacher(id: number): Observable<Teacher> {
        const url = '${this.teachersUrl}/${id}';
        return this.http.get<Teacher>(url)
        .pipe(
            catchError(this.handleError<Teacher>('get teacher'))
        );
    }

    addTeacher(teacher: Teacher): Observable<Teacher> {
        return this.http.post<Teacher>(this.teachersUrl, teacher, this.httpOptions)
        .pipe(
            catchError(this.handleError<Teacher>('add teacher'))
        );
    }

    updateTeacher(teacher: Teacher): Observable<any> {
        return this.http.post<Teacher>(this.teachersUrl, teacher, this.httpOptions)
        .pipe(
            catchError(this.handleError<any>('update teacher'))
        );
    }

    deleteTeacher(teacher: Teacher | number): Observable<Teacher> {
        const id = typeof teacher === 'number' ? teacher : teacher.id;
        const url = '${this.teachersUrl}/${id}';
        return this.http.delete<Teacher>(url, this.httpOptions)
        .pipe(
            catchError(this.handleError<Teacher>('delete teacher'))
        );
    }

    constructor(private http: HttpClient) { }
}
