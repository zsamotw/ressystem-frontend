import { Component, EventEmitter, OnInit, Input, Output, ViewChild } from '@angular/core';
import { Teacher } from '../../models/teacher';
import { Modal } from 'ngx-modal';

@Component({
    selector: 'app-teacher-details',
    templateUrl: './teacher-details.component.html',
    styleUrls: ['./teacher-details.component.scss']
})
export class TeacherDetailsComponent implements OnInit {

    @Input() teacher: Teacher;
    @Output() teacherToUpdate = new EventEmitter<any>();
    @ViewChild('updateTeacherModal') modal: Modal;

    openTeacherUpdateModal() {
        this.modal.open();
    }

    sendTeacherToUpdate(teacher: Teacher): void {
        this.teacherToUpdate.emit(teacher);
        this.modal.close();
    }

    constructor() { }

    ngOnInit() {
    }
}
