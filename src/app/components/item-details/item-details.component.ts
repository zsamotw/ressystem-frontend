import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Item } from 'src/app/models/item';

@Component({
    selector   : 'app-item-details',
    templateUrl: './item-details.component.html',
    styleUrls  : ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {

    @Input() item: Item;
    @Output() itemToUpdate = new EventEmitter<any>();

    sendItemToUpdate(item: Item): void {
        this.itemToUpdate.emit(item);
    }
    hlao(item: string): number {
        /// <reference path="" />
        return 1;
    }

    constructor() { }

    ngOnInit() {
    }
}
