import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/item';
import { Workshop } from '../../models/workshop';
import { ItemService } from '../../services/item.service';
import { WorkshopService } from '../../services/workshop.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ConfirmationService } from 'primeng/api';

@Component({
    selector: 'app-workshops',
    templateUrl: './workshops.component.html',
    styleUrls: ['./workshops.component.scss'],
    providers: [ConfirmationService]
})
export class WorkshopsComponent implements OnInit {

    workshops$: Observable<Workshop[]>;
    term: string;
    isAddingToCalendar = false;
    private searchTerms = new Subject<string>();

    getWorkshops(): void {
        this.workshops$ = this.workshopService.getWorkshops();
    }

    addWorkshop(dataFromForm: any): void {
        this.workshopService.addWorkshop(dataFromForm as Workshop);
    }

    updateWorkshop(workshop: any): void {
        this.workshopService.updateWorkshop(workshop as Workshop); //.subscribe(w => {
    }

    deleteWorkshop(workshop: Workshop): void {
        this.confirmationService.confirm({
            message: `Are you sure you want to delete workshop: ${workshop.title}`,
            accept: () => {
                this.workshopService.deleteWorkshop(workshop);
            }
        });
    }

    searchWorkshops(query: string) {
        this.searchTerms.next(query);
    }

    clearSearch() {
        this.searchTerms.next('');
        this.term = '';
    }

    addItemToCalendar(item: any): void {
        this.itemService.addItem(item as Item).subscribe(i => {
            console.log(i);
        });
    }

    constructor(private workshopService: WorkshopService, private itemService: ItemService, private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.workshops$ = this.searchTerms.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap((query: string) => this.workshopService.searchWorkshop(query)),
        );
    }
}
