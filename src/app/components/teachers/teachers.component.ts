import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../../services/teacher.service';
import { Teacher } from '../../models/teacher';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss'],
  providers: [ConfirmationService]
})
export class TeachersComponent implements OnInit {
  teachers: Teacher[] = [];
  selectedTeacher: Teacher = this.teachers[0];

  getTeachers(): void {
    this.teacherService
      .getTeachers()
      .subscribe(teachers => (this.teachers = teachers));
  }

  selectTeacher(teacher: Teacher): void {
    this.selectedTeacher = teacher;
  }

  addTeacher(dataFromForm: any): void {
    this.teacherService.addTeacher(dataFromForm as Teacher).subscribe(t => {
      this.teachers.push(t);
    });
  }

  updateTeacher(teacher: any): void {
    this.teacherService.updateTeacher(teacher as Teacher).subscribe(t => {
    });
  }

  deleteTeacher(teacher: Teacher): void {
    this.confirmationService.confirm({
      message: `Are you sure you want to delete teacher: ${teacher.firstName} ${teacher.lastName}`,
      accept: () => {
        this.teachers = this.teachers.filter(t => t !== teacher);
        this.teacherService.deleteTeacher(teacher);
      }
    });
  }

  constructor(private teacherService: TeacherService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getTeachers();
  }
}
