import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { Item } from '../../models/item';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
    items: Item[] = [];
    selectedItem: Item;
    calendarOptions: Options;
    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

    getItems(): void {
        this.itemService.getItems()
        .subscribe(items => { this.items = items; console.log(this.items); });
        console.log(this.items);
    }

    selectItem(item: Item): void {
        console.log(item);
        this.selectedItem = item;
    }

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        this.getItems();
        this.itemService.getItems().subscribe(data => {
            this.calendarOptions = {
                editable: true,
                eventLimit: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth'
                },
                selectable: true,
                events: [],
            };
        });
    }
}
