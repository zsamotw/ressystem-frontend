import { Component, OnInit, Input } from '@angular/core';
import { Workshop } from '../../models/workshop';

@Component({
    selector   : 'app-workshop',
    templateUrl: './workshop.component.html',
    styleUrls  : ['./workshop.component.scss']
})
export class WorkshopComponent implements OnInit {

    @Input() workshop: Workshop;

    get getWorkshopTitle() { return this.workshop ? this.workshop.title : ''; }
    get getWorkshopDescription() { return this.workshop ? this.workshop.description : ''; }

    constructor() { }

    ngOnInit() {
    }

}
