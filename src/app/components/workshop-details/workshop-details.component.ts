import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { Workshop } from '../../models/workshop';
import { Modal } from 'ngx-modal';

@Component({
    selector: 'app-workshop-details',
    templateUrl: './workshop-details.component.html',
    styleUrls: ['./workshop-details.component.scss']
})

export class WorkshopDetailsComponent implements OnInit {

    @Input() workshop: Workshop;
    @Output() workshopToUpdate = new EventEmitter<any>();
    @ViewChild('updateWorkshopModal') modal: Modal;

    openWorkshopUpdateModal() {
        this.modal.open();
    }

    sendWorkshopToUpdate(workshop: Workshop): void {
        this.workshopToUpdate.emit(workshop);
        this.modal.close();
    }

    constructor() { }

    ngOnInit() {
    }
}
