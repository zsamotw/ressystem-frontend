import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {

  selected = '';

  select(item: string): void {
    this.selected = item;
    console.log(this.selected);
  }

  constructor() { }

  ngOnInit() {
  }

}
