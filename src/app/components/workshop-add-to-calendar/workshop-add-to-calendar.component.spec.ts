import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopAddToCalendarComponent } from './workshop-add-to-calendar.component';

describe('WorkshopAddToCalendarComponent', () => {
  let component: WorkshopAddToCalendarComponent;
  let fixture: ComponentFixture<WorkshopAddToCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopAddToCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopAddToCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
