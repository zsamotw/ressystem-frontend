import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { TeacherService } from '../../services/teacher.service';
import { Teacher } from '../../models/teacher';
import { Workshop } from '../../models/workshop';
import { Modal } from 'ngx-modal';

@Component({
    selector: 'app-workshop-add-to-calendar',
    templateUrl: './workshop-add-to-calendar.component.html',
    styleUrls: ['./workshop-add-to-calendar.component.scss']
})
export class WorkshopAddToCalendarComponent implements OnInit {
    @Input() workshop: Workshop;
    @Output() itemDetailsToSave = new EventEmitter<any>();
    @ViewChild('addToCalendarModal') modal: Modal;

    teachers: Teacher[] = [];

    getTeachers(): void {
        this.teacherService
        .getTeachers()
        .subscribe(teachers => (this.teachers = teachers));
    }

    addToCalendar(): void {
        this.modal.open();
    }

    sendItemDetails(
        teacher: any,
        date: Date,
        location: string,
        firstName: string,
        lastName: string,
        email: string,
        phone: string,
        institutionName: string,
        institutionAddress: string,
        childrenAge: number,
        additionalData: string
    ): void {
        console.log(teacher);
        this.itemDetailsToSave.emit({
            workshop: this.workshop,
            teacher: JSON.parse(teacher) as Teacher,
            date: date,
            location: location,
            subscriber: {
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone,
                institutionName: institutionName,
                institutionAddress: institutionAddress,
                childrenAge: childrenAge,
                additionalData: additionalData
            }
        });
        this.modal.close();
    }

    toStr(json: any): string {
        return JSON.stringify(json);
    }

    constructor(private teacherService: TeacherService) {}

    ngOnInit() {
        this.getTeachers();
    }
}
