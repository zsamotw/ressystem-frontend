import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Modal } from 'ngx-modal';

@Component({
    selector: 'app-new-teacher',
    templateUrl: './new-teacher.component.html',
    styleUrls: ['./new-teacher.component.scss']
})
export class NewTeacherComponent implements OnInit {

    @Output() teacherDetailsToSave = new EventEmitter<any>();
    @ViewChild('addNewTeacherModal') modal: Modal;

    openNewTeacherModal() {
        this.modal.open();
    }

    sendTeacherDetails(firstName: string, lastName: string, email: string): void {
        this.teacherDetailsToSave.emit({ firstName: firstName, lastName: lastName, email: email });
        this.modal.close();
    }

    constructor() { }

    ngOnInit() {
    }
}
