import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Teacher } from 'src/app/models/teacher';

@Component({
  selector   : 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls  : ['./teacher.component.scss']
})

export class TeacherComponent implements OnInit {

  @Input() teacher: Teacher;

  get getTeacherFirstName() { return this.teacher ? this.teacher.firstName : ''; }
  get getTeacherLastName() { return this.teacher ? this.teacher.lastName : ''; }
  get getTeacherEmail() { return this.teacher ? this.teacher.email : ''; }

  constructor() { }

  ngOnInit() {
  }

}


