import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../models/item';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

    @Input() item: Item;

    get getWorkshopTitle() { return this.item.workshop ? this.item.workshop.title : ''; }
    get getWorkshopDescription() { return this.item.workshop ? this.item.workshop.description : ''; }

    get getDate() { return this.item.date ? this.item.date : ''; }
    get getLocation() { return this.item.location ? this.item.location : ''; }

    get getTeacherFirstName() { return this.item.teacher ? this.item.teacher.firstName : ''; }
    get getTeacherLastName() { return this.item.teacher ? this.item.teacher.lastName : ''; }
    get getTeacherEmail() { return this.item.teacher ? this.item.teacher.email : ''; }

    get getSubscriberFirstName() { return !!this.item.subscriber ? this.item.subscriber.firstName : ''; }
    get getSubscriberLastName() { return !!this.item.subscriber ? this.item.subscriber.lastName : ''; }
    get getSubscriberEmail() { return !!this.item.subscriber ? this.item.subscriber.email : ''; }
    get getSubscriberPhone() { return !!this.item.subscriber ? this.item.subscriber.phone : ''; }
    get getSubscriberInstitutionName() { return !!this.item.subscriber ? this.item.subscriber.institutionName : ''; }
    get getSubscriberInstitutionAddress() { return !!this.item.subscriber ? this.item.subscriber.institutonAddress : ''; }
    get getSubscriberChildrenAge() { return !!this.item.subscriber ? this.item.subscriber.childrenAge : ''; }
    get getSubscriberAdditionalData() { return !!this.item.subscriber ? this.item.subscriber.additionalData : ''; }

    constructor() { }

    ngOnInit() {
    }
}
