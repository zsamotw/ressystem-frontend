import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Modal } from 'ngx-modal';

@Component({
    selector: 'app-new-workshop',
    templateUrl: './new-workshop.component.html',
    styleUrls: ['./new-workshop.component.scss']
})
export class NewWorkshopComponent implements OnInit {

    @Output() workshopDetailsToSave = new EventEmitter<any>();
    @ViewChild('addNewWorkshopModal') modal: Modal;

    openNewWorkshopModal() {
        this.modal.open();
    }

    sendWorkshopDetails(title: string, description: string): void {
        this.workshopDetailsToSave.emit({ title: title, description: description });
        this.modal.close();
    }

    constructor() { }

    ngOnInit() {
    }
}

