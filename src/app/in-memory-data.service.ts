import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Teacher } from './models/teacher';
import { Workshop } from './models/workshop';
import { Item } from './models/item';

@Injectable({
    providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

    createDb() {
        const teachers: Teacher[] = [
            { id: 1, firstName: 'jola', lastName: 'wiech', email: 'jolaatfoal' },
            { id: 2, firstName: 'mariola', lastName: 'natola', email: 'natolatfoal' },
            { id: 3, firstName: 'izola', lastName: 'fifola', email: 'fifolatfoal' },
        ];
        const workshops: Workshop[] = [
            { id: 1, title: 'workshop1', description: 'cos taml' },
            { id: 2, title: 'workshop2', description: 'Halo to ja i ty' },
            { id: 3, title: 'workshop3', description: 'barszo wazny warsztat dla wszystkichh' }
        ];
        const items: Item[] = [
            { id: 1, workshop: workshops[0], date: new Date(), location: 'gdzies', teacher: teachers[0], subscriber: null },
            { id: 2, workshop: workshops[1], date: new Date(), location: 'gdzies', teacher: teachers[1], subscriber: null },
            { id: 3, workshop: workshops[2], date: new Date(), location: 'gdzies', teacher: teachers[2], subscriber: null }
        ];
        return {teachers, workshops, items};
    }

    constructor() { }
}
