import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeachersComponent } from './components/teachers/teachers.component';
import { WorkshopsComponent } from './components/workshops/workshops.component';
import { ItemsComponent } from './components/items/items.component';

const routes: Routes = [
	{ path: 'teachers', component: TeachersComponent },
	{ path: 'workshops', component: WorkshopsComponent },
	{ path: 'items', component: ItemsComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
