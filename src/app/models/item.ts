import { Workshop } from './workshop';
import { Teacher } from './teacher';
import { Subscriber } from './subscriber';

export class Item {
    id: number;
    workshop: Workshop;
    date: Date;
    location: string;
    teacher: Teacher;
    subscriber: Subscriber;
}
