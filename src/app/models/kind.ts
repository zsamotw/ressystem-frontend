export enum Kind {
    forIndividual = 'For individuals',
    forInstitution = 'For institutions'
}