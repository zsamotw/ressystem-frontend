export enum ChildrenCategory {
    TwoToThree = 'Kindergarten',
        FourToSix = 'Primary school',
        SevenToTen = 'High School',
        TenToEighteen = 'Ten to Eighteen',
        ForAll = 'For all',
        NotCategorized = 'Not categorized'
}
