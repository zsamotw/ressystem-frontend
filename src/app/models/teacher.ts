import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

export class Teacher {
    id: number;
    firstName: string;
    lastName: string;
    email: string;

    constructor(id: number, firstName: string, lastName: string, email: string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}
