export interface Subscriber {
    firstName         : string;
    lastName          : string;
    email             : string;
    phone             : number;
    institutionName?  : string;
    institutonAddress?: string;
    childrenAge       : number;
    additionalData    : string;
}
