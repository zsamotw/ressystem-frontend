import { Kind } from './kind';
import { ChildrenCategory } from './childrenCategory';

export class Workshop {
    id: number;
    title: string;
    description: string;
}
